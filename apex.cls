//regular stuff to send a mail

Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
String[] toAddresses = new String[] {'npsgoud@gmail.com'}; 
String[] ccAddresses = new String[] {'smith@gmail.com'};
mail.setToAddresses(toAddresses);
mail.setCcAddresses(ccAddresses);
mail.setReplyTo('support@acme.com');
mail.setSenderDisplayName('Salesforce Support');
mail.setSubject('New Case Created');
mail.setPlainTextBody('Your Case has been created.');

// Attaching starts here

    mail.setHtmlBody('Your case has been created.<p>'+'To view your case <a href=https://.salesforce.com>click here.</a>');
    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
    attach.setContentType('application/pdf');
    attach.setFileName('Test');
    attach.setInline(true);
    String body='Your case has been created.<p>'+'To view your case <a href=https://.salesforce.com>click here.</a>';
    attach.Body = Blob.toPDF(body);
    mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach});
    
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });